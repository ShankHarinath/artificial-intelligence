import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>Reversi Game Implementation</h1>
 * 
 * The Search program implements different graph search algorithms
 * 
 * Greedy Implementation
 * MiniMax Implementation
 * Alpha-Beta Pruning Implementation
 *
 * @author  Shashank Harinath
 * @since   2014-10-13 
 */

@SuppressWarnings({"unused"})
public class agent {

	public static final int GREADY_IMPLEMENTATION = 1;
	public static final int MINIMAX_IMPLEMENTATION = 2;
	public static final int ALPHA_BETA_IMPLEMENTATION = 3;
	public static final int WARZONE = 4;

	public static final String [][] state = new String[8][8]; 

	public static final int [][] weights ={{99,-8,8,6,6,8,-8,99}, 
		{-8,-24,-4,-3,-3,-4,-24,-8},
		{8,-4,7,4,4,7,-4,8}, 
		{6,-3,4,0,0,4,-3,6},
		{6,-3,4,0,0,4,-3,6}, 
		{8,-4,7,4,4,7,-4,8},
		{-8,-24,-4,-3,-3,-4,-24,-8}, 
		{99,-8,8,6,6,8,-8,99}};

	public static final String[] rowAplhabets = {"a", "b", "c", "d", "e", "f", "g", "h"};

	public static String player, oppositionPlayer, stackTrace = "";
	public static int depth, algoType;

	public static boolean gameOver = false;

	public class Node implements Comparable<Node>, Comparator<String>
	{
		private int depth;
		private String name;
		private String parent;
		private double value;
		private String moves;

		public Node(int depth, String name, String parent, double value, String moves) 
		{
			this.depth = depth;
			this.name = name;
			this.parent = parent;
			this.value = value;
			this.moves = moves;
		}

		public Node() {}

		public int getDepth() 
		{
			return depth;
		}
		public void setDepth(int depth) 
		{
			this.depth = depth;
		}
		public String getName() 
		{
			return name;
		}
		public void setName(String name) 
		{
			this.name = name;
		}
		public String getParent() 
		{
			return parent;
		}
		public void setParent(String parent) 
		{
			this.parent = parent;
		}
		public double getValue() 
		{
			return value;
		}
		public void setValue(double value) 
		{
			this.value = value;
		}
		public String getMoves() 
		{
			return moves;
		}
		public void setMoves(String move) 
		{
			this.moves = move;
		}

		public String toString() 
		{
			return "Name: " + this.name + " :: Parent: " + this.parent + " :: Depth: " 
					+ this.depth + " :: Value: " + this.value + " :: Moves: "+ this.moves +"\n";
		}

		public int compareTo(Node node) 
		{
			int value = (int) (node.getValue() - this.getValue());
			if(this.getMoves().contains("!"))
			{
				return value==0 ? (Integer.parseInt(this.getMoves().split("!")[1].replace(",", "")) 
						- (Integer.parseInt(node.getMoves().split("!")[1].replace(",", "")))): value;
			}
			return value==0 ? (Integer.parseInt(this.getMoves().replace(",", "")) 
					- (Integer.parseInt(node.getMoves().replace(",", "")))): value;
		}

		public int compare(String o1, String o2) 
		{
			return Integer.parseInt(o1.replaceAll(",", "")) - Integer.parseInt(o2.replaceAll(",", ""));
		}
	}

	public static void main(String[] args) throws Exception 
	{
		agent agent = new agent();
		readInput("input.txt");

		switch(algoType)
		{
		case GREADY_IMPLEMENTATION:
			stackTrace = getPrintableState(agent.greedyApproach());
			break;
		case MINIMAX_IMPLEMENTATION: 
			stackTrace = "Node,Depth,Value\n";
			agent.miniMax();
			break;
		case ALPHA_BETA_IMPLEMENTATION:
			stackTrace = "Node,Depth,Value,Alpha,Beta\n";
			agent.alphaBeta();
			break;
		case WARZONE: 
			break;
		default : 
			writeOutput("No Such Algorithm!!");
			break;
		}
		writeOutput(stackTrace);
	}

	public String[][] greedyApproach()
	{
		Set<String> moves = getPossibleMovesForTheState(player, state);
		List<Node> child = new ArrayList<Node>();
		int posX, posY;

		String [][] localData = null;

		if(moves.isEmpty())
		{
			return state;
		}

		for(String move : moves)
		{
			localData = clone(state);
			posX = Integer.valueOf(move.split(",")[0]);
			posY = Integer.valueOf(move.split(",")[1]);
			changeState(localData, player, posX, posY);
			child.add(new Node(1, (rowAplhabets[posY]+(posX+1)), "root", evaluate(localData), (posX+","+posY)));
		}

		Collections.sort(child);

		posX = Integer.valueOf(child.get(0).getMoves().split(",")[0]);
		posY = Integer.valueOf(child.get(0).getMoves().split(",")[1]);

		localData = clone(state);
		changeState(localData, player, posX, posY);

		return localData;
	}

	public double miniMax()
	{
		Node root = new Node(0, "root", "root", Double.NEGATIVE_INFINITY, "root");
		return maxVal(state, root, player);
	}

	public double minVal(String [][] state, Node node, String player)
	{
		List<Node> children = new ArrayList<Node>();
		String opposition = ((player.equals("X"))? "O" : "X");
		String [][] nextMove = null;
		int posX, posY;
		List<Node> movesList = new ArrayList<agent.Node>();
		Node child = null;
		String trace = "";

		double evaluationValue = Double.POSITIVE_INFINITY;

		if(node.getDepth()==depth || isBoardFull())
		{
			evaluationValue = evaluate(state);
			stackTrace += node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)+"\n";
			if(isBoardFull())
			{
				returnState(node, children, evaluationValue);
			}
			return evaluationValue;
		}

		List<String> moves = new ArrayList<String>();
		moves.addAll(getPossibleMovesForTheState(player, state));
		Collections.sort(moves, new Node());

		trace = node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)+"\n";

		if(moves.isEmpty())
		{
			if(isGameOver(state, player) || isGameOver(state, opposition)
					|| (node.getParent().equalsIgnoreCase("pass") && node.getName().equalsIgnoreCase("pass")))
			{
				evaluationValue = evaluate(state);
				trace = node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)+"\n";

				stackTrace += trace;
				returnState(node, children, evaluationValue);
				return evaluationValue;
			}

			trace = (!(node.getName().length()==0)?node.getName()+"":"pass")+","+node.getDepth()
					+","+getPrintableValue(evaluationValue)+"\n";
			stackTrace += trace;

			child = new Node(node.getDepth()+1, "pass", node.getName(), 
					Double.NEGATIVE_INFINITY, node.getMoves()+"!");

			evaluationValue = maxVal(state, child, opposition);

			//In case of Pass state, printing after it returns from the child evaluation
			trace = (!(node.getName().length()==0)?node.getName()+"":"pass")+","+node.getDepth()
					+","+getPrintableValue(evaluationValue)+"\n";
			stackTrace += trace;

			returnState(node, children, evaluationValue);
			return evaluationValue;
		}

		stackTrace += trace;

		for(String move : moves)
		{
			nextMove = clone(state);
			posX = Integer.valueOf(move.split(",")[0]);
			posY = Integer.valueOf(move.split(",")[1]);

			changeState(nextMove, player, posX, posY);

			child = new Node(node.getDepth()+1, (rowAplhabets[posY]+(posX+1)), node.getName(), 
					Double.NEGATIVE_INFINITY, node.getMoves()+"!"+(posX+","+posY));
			child.setValue(maxVal(nextMove, child, opposition));

			children.add(child);

			if(evaluationValue>child.getValue())
			{
				evaluationValue = child.getValue();
				node.setValue(evaluationValue);
			}

			stackTrace += node.getName()+","+node.getDepth()+","+getPrintableValue(node.getValue())+"\n";

			if(gameOver)
			{
				returnState(node, children, evaluationValue);
				return node.getValue();
			}
		}

		returnState(node, children, evaluationValue);

		return evaluationValue;
	}

	public double maxVal(String[][] state, Node node, String player)
	{
		List<Node> children = new ArrayList<Node>();;
		String opposition = ((player.equals("X"))? "O" : "X");
		String [][] nextMove = null;
		int posX, posY;
		List<Node> movesList = new ArrayList<agent.Node>();
		Node child = null;
		String trace = "";

		double evaluationValue = Double.NEGATIVE_INFINITY;

		if(node.getDepth()==depth || isBoardFull())
		{
			evaluationValue = evaluate(state);
			stackTrace += node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)+"\n";

			if(isBoardFull())
			{
				returnState(node, children, evaluationValue);
			}
			return evaluationValue;
		}

		List<String> moves = new ArrayList<String>();
		moves.addAll(getPossibleMovesForTheState(player, state));
		Collections.sort(moves, new Node());

		trace = node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)+"\n";

		if(moves.isEmpty())
		{
			if(isGameOver(state, player) || isGameOver(state, opposition)
					|| (node.getParent().equalsIgnoreCase("pass") && node.getName().equalsIgnoreCase("pass")))
			{
				evaluationValue = evaluate(state);
				trace = node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)+"\n";

				stackTrace += trace;

				returnState(node, children, evaluationValue);
				return evaluationValue;
			}

			trace = (!(node.getName().length()==0)?node.getName()+"":"pass")+","+node.getDepth()
					+","+getPrintableValue(evaluationValue)+"\n";
			stackTrace += trace;

			child = new Node(node.getDepth()+1, "pass", node.getName(), 
					Double.POSITIVE_INFINITY, node.getMoves()+"!");

			evaluationValue = minVal(state, child, opposition);

			//In case of Pass state, printing after it returns from the child evaluation
			trace = (!(node.getName().length()==0)?node.getName()+"":"pass")+","+node.getDepth()
					+","+getPrintableValue(evaluationValue)+"\n";
			stackTrace += trace;

			returnState(node, children, evaluationValue);

			return evaluationValue;
		}

		stackTrace += trace;

		for(String move : moves)
		{
			nextMove = clone(state);
			posX = Integer.valueOf(move.split(",")[0]);
			posY = Integer.valueOf(move.split(",")[1]);

			changeState(nextMove, player, posX, posY);

			child = new Node(node.getDepth()+1, (rowAplhabets[posY]+(posX+1)), node.getName(), 
					Double.POSITIVE_INFINITY, node.getMoves()+"!"+(posX+","+posY));
			child.setValue(minVal(nextMove, child, opposition));
			children.add(child);

			if(evaluationValue<child.getValue())
			{
				evaluationValue = child.getValue();
				node.setValue(evaluationValue);
			}

			stackTrace += node.getName()+","+node.getDepth()+","+getPrintableValue(node.getValue())+"\n";

			if(gameOver)
			{
				returnState(node, children, evaluationValue);
				return node.getValue();
			}
		}

		returnState(node, children, evaluationValue);

		return evaluationValue;
	}

	public double alphaBeta()
	{
		Node root = new Node(0, "root", "root", Double.NEGATIVE_INFINITY, "root");
		return maxVal(state, root, player, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
	}

	public double minVal(String [][] state, Node node, String player, double alpha, double beta)
	{
		List<Node> children = new ArrayList<Node>();
		String opposition = ((player.equals("X"))? "O" : "X");
		String [][] nextMove = null;
		int posX, posY;
		List<Node> movesList = new ArrayList<agent.Node>();
		Node child = null;
		String trace = "";

		double evaluationValue = Double.POSITIVE_INFINITY;

		if(node.getDepth()==depth || isBoardFull())
		{
			evaluationValue = evaluate(state);
			trace = node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)
					+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

			stackTrace += trace;

			if(isBoardFull())
			{
				returnState(node, children, evaluationValue);
			}
			return evaluationValue;
		}

		List<String> moves = new ArrayList<String>();
		moves.addAll(getPossibleMovesForTheState(player, state));
		Collections.sort(moves, new Node());

		trace = node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)
				+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

		if(moves.isEmpty())
		{
			// First condition to check if the player doesnt have any move
			// No moves for both players
			if(isGameOver(state, player) || isGameOver(state, opposition) 
					|| (node.getParent().equalsIgnoreCase("pass") && node.getName().equalsIgnoreCase("pass")))
			{
				evaluationValue = evaluate(state);
				trace = node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)
						+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";
				stackTrace += trace;
				returnState(node, children, evaluationValue);
				return evaluationValue;
			}

			trace = (!(node.getName().length()==0)?node.getName()+",":"pass,")+node.getDepth()+","
					+getPrintableValue(evaluationValue)+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

			stackTrace += trace;

			child = new Node(node.getDepth()+1, "pass", node.getName(), 
					Double.NEGATIVE_INFINITY, node.getMoves()+"!");

			//In case of Pass state, print after it returns from the child evaluation
			evaluationValue = maxVal(state, child, opposition, alpha, beta);

			//Update Beta and don update if it is getting pruned
			if(evaluationValue < beta && !(evaluationValue <= alpha))
			{
				beta = evaluationValue;
			}

			trace = (!(node.getName().length()==0)?node.getName()+",":"pass,")+node.getDepth()
					+","+getPrintableValue(evaluationValue)+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

			stackTrace += trace;
			returnState(node, children, evaluationValue);

			return evaluationValue;
		}

		stackTrace += trace;

		for(String move : moves)
		{
			nextMove = clone(state);
			posX = Integer.valueOf(move.split(",")[0]);
			posY = Integer.valueOf(move.split(",")[1]);

			changeState(nextMove, player, posX, posY);

			child = new Node(node.getDepth()+1, (rowAplhabets[posY]+(posX+1)), node.getName(), 
					Double.NEGATIVE_INFINITY, node.getMoves()+"!"+(posX+","+posY));
			child.setValue(maxVal(nextMove, child, opposition, alpha, beta));
			children.add(child);

			//Min(eval,maxVal(children))
			if(evaluationValue>child.getValue())
			{
				evaluationValue = child.getValue();
				node.setValue(evaluationValue);
			}

			//Update Beta and don update if it is getting pruned
			if(evaluationValue < beta && !(evaluationValue <= alpha))
			{
				beta = evaluationValue;
			}

			stackTrace += node.getName()+","+node.getDepth()+","+getPrintableValue(node.getValue())
					+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

			//Pruning condition
			if(evaluationValue <= alpha)
			{
				returnState(node, children, evaluationValue);
				return evaluationValue;
			}


			if(gameOver)
			{
				returnState(node, children, evaluationValue);
				return node.getValue();
			}
		}

		returnState(node, children, evaluationValue);

		return evaluationValue;
	}

	public double maxVal(String [][] state, Node node, String player, double alpha, double beta)
	{
		List<Node> children = new ArrayList<Node>();;
		String opposition = ((player.equals("X"))? "O" : "X");
		String [][] nextMove = null;
		int posX, posY;
		List<Node> movesList = new ArrayList<agent.Node>();
		Node child = null;
		String trace = "";

		double evaluationValue = Double.NEGATIVE_INFINITY;

		if(node.getDepth()==depth || isBoardFull())
		{
			evaluationValue = evaluate(state);
			stackTrace += node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)
					+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

			if(isBoardFull())
			{
				returnState(node, children, evaluationValue);
			}
			return evaluationValue;
		}

		List<String> moves = new ArrayList<String>();
		moves.addAll(getPossibleMovesForTheState(player, state));
		Collections.sort(moves, new Node());

		trace = node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)
				+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

		if(moves.isEmpty())
		{
			if(isGameOver(state, player) || isGameOver(state, opposition) 
					|| (node.getParent().equalsIgnoreCase("pass") && node.getName().equalsIgnoreCase("pass")))
			{
				evaluationValue = evaluate(state);
				trace = node.getName()+","+node.getDepth()+","+getPrintableValue(evaluationValue)
						+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

				stackTrace += trace;
				returnState(node, children, evaluationValue);
				return evaluationValue;
			}

			trace = (!(node.getName().length()==0)?node.getName()+",":"pass,")+node.getDepth()
					+","+getPrintableValue(evaluationValue)+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

			stackTrace += trace;

			child = new Node(node.getDepth()+1, "pass", node.getName(), 
					Double.POSITIVE_INFINITY, node.getMoves()+"!");

			//In case of Pass state, print after it returns from the child evaluation
			evaluationValue = minVal(state, child, opposition, alpha, beta);

			//Update Alpha && don update in case if it is getting pruned
			if(evaluationValue > alpha && !(evaluationValue >= beta))
			{
				alpha = evaluationValue;
			}

			trace = (!(node.getName().length()==0)?node.getName()+",":"pass,")+node.getDepth()
					+","+getPrintableValue(evaluationValue)+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

			stackTrace += trace;
			returnState(node, children, evaluationValue);

			return evaluationValue;
		}

		stackTrace += trace;

		for(String move : moves)
		{
			nextMove = clone(state);
			posX = Integer.valueOf(move.split(",")[0]);
			posY = Integer.valueOf(move.split(",")[1]);

			changeState(nextMove, player, posX, posY);

			child = new Node(node.getDepth()+1, (rowAplhabets[posY]+(posX+1)), node.getName(), 
					Double.POSITIVE_INFINITY, node.getMoves()+"!"+(posX+","+posY));
			child.setValue(minVal(nextMove, child, opposition, alpha, beta));
			children.add(child);

			//Max(eval,minVal(children))
			if(evaluationValue<child.getValue())
			{
				evaluationValue = child.getValue();
				node.setValue(evaluationValue);
			}

			//Update Alpha && don update in case if it is getting pruned
			if(evaluationValue > alpha && !(evaluationValue >= beta))
			{
				alpha = evaluationValue;
			}

			stackTrace += node.getName()+","+node.getDepth()+","+getPrintableValue(node.getValue())
					+","+getPrintableValue(alpha)+","+getPrintableValue(beta)+"\n";

			//Pruning condition
			if(evaluationValue >= beta)
			{
				returnState(node, children, evaluationValue);
				return evaluationValue;
			}

			if(gameOver)
			{
				returnState(node, children, evaluationValue);
				return node.getValue();
			}
		}

		returnState(node, children, evaluationValue);

		return evaluationValue;
	}

	public static void returnState(Node node, List<Node> children, double evaluationValue)
	{
		if(node.getName().equalsIgnoreCase("Root") && !(stackTrace.contains("*")))
		{
			if(children.isEmpty())
			{
				stackTrace = getPrintableState(state).trim() + "\n" + stackTrace;;
				return;
			}
			Collections.sort(children);

			for(Node n : children)
			{
				if(n.getValue()==evaluationValue)
				{
					stackTrace = getPrintableState(generateState(n, player)).trim() + "\n" + stackTrace;
					break;
				}
			}
		}
	}

	public static boolean isGameOver(String[][] state, String player) 
	{
		String row = "";
		for(int i=0; i< 8; i++)
		{
			row = Arrays.toString(state[i]).substring(1, Arrays.toString(state[i]).length()-1);
			if(row.contains(player))
			{
				return false;
			}
		}
		gameOver = true;
		return true;
	}

	public static boolean isBoardFull() 
	{
		String row = "";
		for(int i=0; i< 8; i++)
		{
			row = Arrays.toString(state[i]).substring(1, Arrays.toString(state[i]).length()-1);
			if(row.contains("*"))
			{
				return false;
			}
		}
		gameOver = true;
		return true;
	}


	public static void changeState(String[][] state, String player, int posX, int posY)
	{
		state[posX][posY] = "@";
		flip(state, posX, posY, player);
		state[posX][posY] = player;
	}

	public static String[][] generateState(Node node, String player)
	{
		String[][] childState = clone(state);
		String opposition = ((player.equals("X"))? "O" : "X");

		for(String positions : node.getMoves().split("!"))
		{
			if(positions.contains("root"))
			{
				continue;
			}
			if(positions.contains(","))
			{
				changeState(childState, player, Integer.parseInt(positions.split(",")[0]), Integer.parseInt(positions.split(",")[1]));
				player = ((player.equals("X"))? "O" : "X");
			}
			else
			{
				player = ((player.equals("X"))? "O" : "X");
			}
		}

		return childState;
	}

	public static void flip(String[][] state, int posX, int posY, String player)
	{
		//Horizontal and Main diagonal
		partialFlip(state, posX, posY, player);
		rotateArrayToRight(state);

		//Vertical and Anti-diagonal
		partialFlip(state, posY, 7-posX, player);
		rotateArrayToLeft(state);
	}

	public static void partialFlip(String[][] state, int posX, int posY, String player)
	{
		//		String player = state[posX][posY];
		String opposition = ((player.equals("X"))? "O" : "X");

		Pattern first = Pattern.compile("@"+opposition+"+?"+player);
		Pattern end = Pattern.compile(player+opposition+"+?"+"@");

		String row = "", diagonal = "", replace = "";
		Matcher matcher = null;

		row = Arrays.toString(state[posX]).substring(1, Arrays.toString(state[posX]).length()-1).replaceAll(",|\\s", "");
		matcher = first.matcher(row);

		while (matcher.find()) 
		{
			for(int i= matcher.start()+1; i<matcher.end()-1; i++)
			{
				state[posX][i] = player;
				row = Arrays.toString(state[posX]).substring(1, Arrays.toString(state[posX]).length()-1).replaceAll(",|\\s", "");
			}
		}

		matcher = end.matcher(row);

		while (matcher.find()) 
		{
			for(int i= matcher.start()+1; i<matcher.end()-1; i++)
			{
				state[posX][i] = player;
				row = Arrays.toString(state[posX]).substring(1, Arrays.toString(state[posX]).length()-1).replaceAll(",|\\s", "");
			}
		}

		if(posX-posY>0)
		{
			for(int j=0;j<8 && Math.abs(posX-posY)+j<8;j++)
			{
				diagonal += state[Math.abs(posX-posY)+j][j];
			}
		}
		else
		{
			for(int j=0;j<8 && Math.abs(posX-posY)+j<8;j++)
			{
				diagonal += state[j][Math.abs(posX-posY)+j];
			}
		}
		matcher = first.matcher(diagonal);

		while (matcher.find()) 
		{
			for(int i=matcher.start();i<matcher.end();i++)
			{
				replace += player;
			}
			diagonal = matcher.replaceAll(replace);
			if(posX-posY>0)
			{
				for(int j=0;j<8 && Math.abs(posX-posY)+j<8;j++)
				{
					try{
						if(!state[Math.abs(posX-posY)+j][j].equals("@"))
						{
							state[Math.abs(posX-posY)+j][j] = "" + diagonal.charAt(j);
						}
					}catch(Exception e){}
				}
			}
			else
			{
				for(int j=0;j<8 && Math.abs(posX-posY)+j<8;j++)
				{
					try{
						if(!state[j][Math.abs(posX-posY)+j].equals("@"))
						{
							state[j][Math.abs(posX-posY)+j] = "" + diagonal.charAt(j);
						}
					}
					catch (Exception e) {}
				}
			}	
		}

		matcher = end.matcher(diagonal);

		while (matcher.find()) 
		{
			for(int i=matcher.start();i<matcher.end();i++)
			{
				replace += player;
			}
			diagonal = matcher.replaceAll(replace);
			if(posX-posY>0)
			{
				for(int j=0;j<8 && Math.abs(posX-posY)+j<8;j++)
				{
					try{
						if(!state[Math.abs(posX-posY)+j][j].equals("@"))
						{
							state[Math.abs(posX-posY)+j][j] = "" + diagonal.charAt(j);
						}
					}catch(Exception e){}
				}
			}
			else
			{
				for(int j=0;j<8 && Math.abs(posX-posY)+j<8;j++)
				{
					try{
						if(!state[j][Math.abs(posX-posY)+j].equals("@"))
						{
							state[j][Math.abs(posX-posY)+j] = "" + diagonal.charAt(j);
						}
					}
					catch (Exception e) {}
				}
			}	
		}
	}

	public static Set<String> getPossibleMovesForTheState(String player, String[][] state)
	{
		String indexes = "";
		Set<String> indexList = new HashSet<String>();
		int localIndex;
		String opposition = ((player.equals("X"))? "O" : "X");

		String [][] localData = clone(state);

		indexList.addAll(getHorizontallyValidMoves(player, 0, localData));
		indexList.addAll(getDiagonallyValidMoves(player, 0, localData));
		rotateArrayToRight(localData);

		indexList.addAll(getHorizontallyValidMoves(player, 1, localData));
		indexList.addAll(getDiagonallyValidMoves(player, 1, localData));
		rotateArrayToRight(localData);

		indexList.addAll(getDiagonallyValidMoves(player, 2, localData));
		rotateArrayToRight(localData);

		indexList.addAll(getDiagonallyValidMoves(player, 3, localData));

		return indexList;
	}

	private static Set<String> getHorizontallyValidMoves(String player, int rotation, String[][] state)
	{
		String opposition = ((player.equals("X"))? "O" : "X");
		Pattern pattern = Pattern.compile("@"+opposition+"+?"+player);
		Set<String> indexList = new HashSet<String>();
		String row = "";
		Matcher matcher = null;

		for(int i=0; i< 8; i++)
		{
			row = Arrays.toString(state[i]).substring(1, Arrays.toString(state[i]).length()-1).replaceAll(",|\\s", "").replaceAll("\\*", "@");
			matcher = pattern.matcher(row);

			while (matcher.find()) 
			{
				if(rotation == 0)
				{
					indexList.add(i+","+(matcher.start()));
				}
				else if(rotation == 1)
				{
					indexList.add((7-matcher.start())+","+i);
				}
			}

			matcher = pattern.matcher(new StringBuilder(row).reverse());

			while (matcher.find()) 
			{
				if(rotation == 0)
				{
					indexList.add((i)+","+(7-matcher.start()));
				}
				else if(rotation == 1)
				{
					indexList.add((matcher.start())+","+(i));
				}
			}
		}
		return indexList;
	}
	private static Set<String> getDiagonallyValidMoves(String player, int rotation, String[][] state)
	{
		String opposition = ((player.equals("X"))? "O" : "X");
		Set<String> indexList = new HashSet<String>();
		Pattern pattern = Pattern.compile("@"+opposition+"+?"+player);
		String diagonal = "";
		Matcher matcher = null;
		int posX, posY;

		for(int index = 5; index>=0; index--)
		{
			diagonal = "";
			for(int j=0;j<8-index;j++)
			{
				diagonal += state[index+j][j];
			}

			matcher = pattern.matcher(diagonal.replaceAll("\\*", "@"));

			while (matcher.find()) 
			{
				posX = index + matcher.start();
				posY = matcher.start();

				if(rotation == 1)
				{
					indexList.add((7-posY)+","+(posX));
				}
				else if(rotation == 2)
				{
					indexList.add((7-posX)+","+(7-posY));
				}
				else if(rotation == 3)
				{
					indexList.add((posY)+","+(7-posX));
				}
				else
				{
					indexList.add(posX+","+posY);
				}
			}
		}

		for(int index = 0; index<=5; index++)
		{
			diagonal = "";
			for(int j=0;j<8-index;j++)
			{
				diagonal += state[j][index+j];
			}

			matcher = pattern.matcher(diagonal.replaceAll("\\*", "@"));

			while (matcher.find()) 
			{
				posY = index + matcher.start();
				posX = matcher.start();

				if(rotation == 1)
				{
					indexList.add((7-posY)+","+(posX));
				}
				else if(rotation == 2)
				{
					indexList.add((7 - posX)+","+(7 - posY));
				}
				else if(rotation == 3)
				{
					indexList.add((posY)+","+(7-posX));
				}
				else
				{
					indexList.add(posX+","+posY);
				}
			}
		}

		return indexList;
	}

	public static int evaluate(String[][] state)
	{
		String opposition = ((player.equals("X"))? "O" : "X");
		int playerTotal = 0;
		int oppositionTotal = 0;
		for(int i=0; i<8; i++)
		{
			for(int j=0; j<8; j++)
			{
				if(state[i][j].equals(player))
				{
					playerTotal += weights[i][j];
				}
				else if(state[i][j].equals(opposition))
				{
					oppositionTotal += weights[i][j];
				}
			}
		}

		return playerTotal - oppositionTotal;
	}

	// Utility Functions
	private static void arrayTranspose(String[][] state) 
	{
		for (int i = 0; i < state.length; i++) 
		{
			for (int j = i; j < state[0].length; j++) 
			{
				String x = state[i][j];
				state[i][j] = state[j][i];
				state[j][i] = x;
			}
		}
	}

	public static void rotateArrayToRight(String[][] state) 
	{
		arrayTranspose(state);

		for (int  j = 0; j < state[0].length/2; j++) 
		{
			for (int i = 0; i < state.length; i++) 
			{
				String x = state[i][j];
				state[i][j] = state[i][state[0].length -1 -j]; 
				state[i][state[0].length -1 -j] = x;
			}
		}
	}

	public static void rotateArrayToLeft(String[][] state) 
	{
		arrayTranspose(state);

		for (int  i = 0; i < state.length/2; i++) 
		{
			for (int j = 0; j < state[0].length; j++) 
			{
				String x = state[i][j];
				state[i][j] = state[state.length -1 -i][j]; 
				state[state.length -1 -i][j] = x;
			}
		}
	}

	public static void printState(String[][] state)
	{
		for (int i = 0; i < state.length; i++) 
		{
			for (int j = 0; j < state[0].length; j++) 
			{
				System.out.print(state[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static String getPrintableState(String[][] state)
	{
		String printState = "";
		for (int i = 0; i < state.length; i++) 
		{
			for (int j = 0; j < state[0].length; j++) 
			{
				printState += state[i][j];
			}
			printState += "\n";
		}
		return printState;
	}

	public static void writeOutput(String output) throws IOException
	{
		PrintWriter fileWriter = new PrintWriter("output.txt");
		fileWriter.print(output);
		fileWriter.close();
	}

	public static void readInput(String inputFile) throws FileNotFoundException
	{
		Map<String, Integer> paths;
		List<String> nodeNames = new ArrayList<String>();
		Scanner in = new Scanner(new File(inputFile));
		String row = "";

		algoType = Integer.parseInt(in.nextLine());
		player = in.nextLine();
		oppositionPlayer = player.equals("X") ? "O" : "X";
		depth = Integer.parseInt(in.nextLine());

		for(int i =0; i<8; i++)
		{
			row = in.next();
			for(int j =0; j<8; j++)
			{
				state[i][j] = String.valueOf(row.charAt(j));
			}
		}
		in.close();
	}

	public static String[][] clone(String[][] state)
	{
		String [][] clone = new String [8][8];
		for(int i=0; i<8; i++)
		{
			System.arraycopy(state[i], 0, clone[i], 0, state[i].length);
			//			clone[i] = Arrays.copyOf(state[i], 8);
			//			for(int j=0;j<8;j++)
			//			{
			//				clone[i][j] = state[i][j];
			//			}
		}
		return clone;
	}

	public String getPrintableValue(double initialValue)
	{
		if(initialValue==Double.NEGATIVE_INFINITY)
		{
			return "-Infinity";
		}
		else if(initialValue==Double.POSITIVE_INFINITY)
		{
			return "Infinity";
		}
		return ""+(int)initialValue;
	}
}