import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class agent {

	public static String QUERY = "";
	public static int clauseCount = 0;
	public static List<String> kb = new ArrayList<String>();
	public static Set<String> usedFacts = new HashSet<String>();
	public static Map<String, Boolean> usedFactsTruth = new HashMap<String, Boolean>();

	public static Set<String> substitutions = new HashSet<String>();
	public static Set<String> predicates = new HashSet<String>();
	public static Set<String> constants = new HashSet<String>();

	static{
		readInput();
	}

	public static void main(String[] args) {
		boolean status = backwardChainingAsk();
//		System.out.println(String.valueOf(status).toUpperCase());
		writeOutput(String.valueOf(status).toUpperCase());
	}

	public static boolean backwardChainingAsk(){
		if(kb.contains(QUERY)){
			return true;
		}
		return backwardChainingOr(QUERY, new ArrayList<String>());
	}

	public static boolean backwardChainingOr(String goal, ArrayList<String> substitutions){
//		System.out.println("OR :: "+goal);

		ArrayList<String> deducingFacts = null;

		if(kb.contains(goal) || (substitute(goal, substitutions).size()>0 && kb.contains(substitute(goal, substitutions).get(0))))
			return true;
		
		deducingFacts = fetchFactsForGoal(goal);
		for(String rule : deducingFacts){
			if(!usedFacts.contains(rule)){
				if(rule.contains("=>"))
				{
					usedFacts.add(rule);
					for(String subs : substitute(goal, substitutions)){
						if(rule.equals(subs)){
							usedFactsTruth.put(rule, Boolean.TRUE);
//							System.out.println("OR :: "+goal + " => true");
							return true;
						}
					}
					substitutions.addAll(unify(rule.split("=>")[1], goal));
					boolean b = backwardChainingAnd(rule.split("=>")[0], substitutions);

					usedFactsTruth.put(rule, b);
//					System.out.println("OR :: "+goal + " => " + b);
					if(goal.equals(QUERY) && b){
						return true;
					}
					return b;
				}
				else
					return kb.contains(rule) || kb.contains(substitute(rule, substitutions).get(0));
			}
			else
				return usedFactsTruth.get(rule);
		}

		return false;
	}

	public static boolean backwardChainingAnd(String goal, ArrayList<String> substitutions){
		String first = "";
		String rest = "";
//		System.out.println("And :: "+goal);

		if(goal.isEmpty())
			return false;

		if(goal.contains("&")){
			first = goal.split("&")[0];
			rest = goal.replace(first+"&", "");

			return (backwardChainingOr(first, substitutions) && backwardChainingAnd(rest, substitutions));
		}
		else{
			return backwardChainingOr(goal, substitutions);
		}
	}

	public static ArrayList<String> substitute(String goal, ArrayList<String> yield){
		ArrayList<String> replacedClauses = new ArrayList<String>();
		for(String substitute : yield){
			replacedClauses.add(replaceVariable(goal, substitute));
		}
		return replacedClauses;
	}

	public static ArrayList<String> unify(String fact, String goal){
		ArrayList<String> substitutions = new ArrayList<String>();
		List<String> params = new ArrayList<String>();
		List<String> params1 = new ArrayList<String>();
		int pos = 0;

		if(fact.equals(goal)) 
			return substitutions;

		if(!fact.split("\\(")[0].equals(goal.split("\\(")[0]))
			return substitutions;

		for(String goalSplit: goal.split("&")){
			for(String factSplit: fact.split("&")){
				params = Arrays.asList(goalSplit.split("\\(")[1].replaceAll("\\)", "").split(","));
				params1 = Arrays.asList(factSplit.split("\\(")[1].replaceAll("\\)", "").split(","));

				if(params.contains("x") && params1.contains("x"))
					continue;

				if(params.size()>1){
					pos = (params.contains("x"))?(params.indexOf("x")+1)%2:(params1.indexOf("x")+1)%2;
					if(!params1.get(pos).equals(params.get(pos)))
						continue;
				}

				for(int i=0; i<params.size();i++){
					if((params.get(i).equals("x") || params1.get(i).equals("x")) 
							&& (!params.get(i).equals(params1.get(i)))){
						substitutions.add((!params.get(i).equals("x"))?params.get(i):params1.get(i));
					}
				}
			}
		}

		return substitutions;
	}

	public static boolean canBeUnified(String goal){
		if(goal.contains("(x") || goal.contains("x)"))
		{
			return true;
		}

		return false;
	}

	public static ArrayList<String> fetchFactsForGoal(String goal){
		ArrayList<String> facts = new ArrayList<String>();
		String goalPredicate = goal.split("\\(")[0];
		ArrayList<String> subs = new ArrayList<String>();
		ArrayList<String> goalSubs = new ArrayList<String>();

		if(isFactAlreadyAccessed(goal))
			return facts;

		for(String rule : kb){
			if(rule.contains("=>") && rule.split("=>")[1].contains(goalPredicate)){
				if(rule.split("=>")[1].equals(goal))
					facts.add(rule);
				else{ 
					subs = substitute(rule, unify(rule.split("=>")[1], goal));
					goalSubs = substitute(goal, unify(rule.split("=>")[1], goal));
					if((!subs.isEmpty() || !goalSubs.isEmpty()) && (subs.get(0).split("=>")[1].equals(goalSubs.get(0))))
						//only if the RHS has variable
						if(rule.split("=>")[1].replaceAll(" ", "").contains("(x") || rule.split("=>")[1].replaceAll(" ", "").contains(",x)") 
								|| rule.split("=>")[1].replaceAll(" ", "").contains("(x)")){
							facts.add((unify(rule.split("=>")[1], goal).size()>0)?substitute(rule, unify(rule.split("=>")[1], goal)).get(0):rule);
						}
						else{
							facts.add(rule);
						}
				}
			}
			else if ((!rule.contains("=>")) && rule.contains(goalPredicate) && hasSameParameters(rule, goal))
				facts.add(rule);
		}
		return facts;
	}

	public static boolean hasSameParameters(String rule, String goal) {
		ArrayList<String> subs = new ArrayList<String>();
		ArrayList<String> goalSubs = new ArrayList<String>();

		subs = substitute(rule, unify(rule, goal));
		goalSubs = substitute(goal, unify(rule, goal));

		if(subs.isEmpty()){
			return false;
		}

		return (rule.split("\\(")[1].equals(goal.split("\\(")[1])
				|| subs.get(0).split("\\(")[1].equals(goalSubs.get(0).split("\\(")[1]));
	}

	public static boolean isFactAlreadyAccessed(String goal) {
		return usedFacts.contains(goal);
	}

	public static String replaceVariable(String goal, String substitute){
		return goal.replaceAll("\\(x", "\\("+substitute).replaceAll("x\\)", "\\("+substitute);
	}

	public static void identifyPossibleSubstitutions() {
		List<String> params = new ArrayList<String>();
		List<String> params1 = new ArrayList<String>();
		for(String fact : predicates){
			for(String fact1 : predicates){
				params.clear();
				params1.clear();
				if(((!fact.equals(fact1))) && (fact.split("\\(")[0].equals(fact1.split("\\(")[0]))){
					params.addAll(Arrays.asList(fact.split("\\(")[1].replaceAll("\\)", "").split(",")));
					params1.addAll(Arrays.asList(fact1.split("\\(")[1].replaceAll("\\)", "").split(",")));
					for(int i=0; i<params.size();i++){
						if((params.get(i).equals("x") || params1.get(i).equals("x")) 
								&& (!params.get(i).equals(params1.get(i)))){
							substitutions.add((constants.contains(params.get(i)))?params.get(i):params1.get(i));
						}
					}
				}
			}
		}
	}

	public static void identifyPredicates() {
		for(String fact : kb){
			fact = fact.replaceAll("=>", "@").replaceAll("&", "@");
			predicates.addAll(Arrays.asList(fact.split("@")));
		}
	}

	public static void identifyConstants() {
		for(String fact : predicates){
			for(String fact1 : predicates){
				if(((!fact.equals(fact1))) && (fact.split("\\(")[0].equals(fact1.split("\\(")[0]))){
					constants.addAll(Arrays.asList(fact.split("\\(")[1].replaceAll("\\)", "").split(",")));
					constants.addAll(Arrays.asList(fact1.split("\\(")[1].replaceAll("\\)", "").split(",")));
				}
			}
		}
		constants.remove("x");
	}

	public static void readInput(){
		Scanner in = null;
		try{
			in = new Scanner(new File("input.txt"));

			QUERY = in.nextLine();
			clauseCount = Integer.parseInt(in.nextLine());

			for(int i =0; i<clauseCount; i++)
				kb.add(in.nextLine());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally{
			in.close();
		}
	}

	public static void writeOutput(String output){
		PrintWriter fileWriter = null;
		try {
			fileWriter = new PrintWriter("output.txt");
			fileWriter.print(output);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally{
			fileWriter.close();
		}
	}
}
