import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

/**
 * <h1>Graph Search</h1>
 * 
 * The Search program implements different graph search algorithms
 * 
 * Breadth First Search
 * Depth First Search
 * Uniform Cost Search
 *
 * @author  Shashank Harinath
 * @version 3.1
 * @since   2014-09-13 
 */

@SuppressWarnings("unchecked")
public class agent {
	
	public static final int BREADTH_FIRST_SEARCH = 1;
	public static final int DEPTH_FIRST_SEARCH = 2;
	public static final int UNIFORM_COST_SEARCH = 3;

	//Main graph will be read into this from the Adjacency graph 
	public static Map<String, Map<String, Integer>> graph = new HashMap<String, Map<String, Integer>>();

	//Queue containing visited nodes in a graph.
	public static Queue<Node> visitedNodes = new LinkedList<Node>();

	public static String source, dest, stackTrace = "";
	public static int cost, searchType;

	//Queue used to save nodes in BFS and UCS --> Queue Searches
	public static Queue<Node> nodeQueue = new LinkedList<Node>();

	//Stack used to save nodes in DFS --> Stack Searches
	public static Stack<Node> nodeStack = new Stack<Node>();

	public class Node implements Comparable<Node>
	{
		private String state;
		private int depth;
		private String parent;
		private int cost;

		public Node(String state, String parent, int depth, int cost)
		{
			this.state = state;
			this.parent = parent;
			this.depth = depth;
			this.cost = cost;
		}

		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public int getDepth() {
			return depth;
		}
		public void setDepth(int depth) {
			this.depth = depth;
		}
		public String getParent() {
			return parent;
		}
		public void setParent(String parent) {
			this.parent = parent;
		}
		public int getCost() {
			return cost;
		}
		public void setCost(int cost) {
			this.cost = cost;
		}

		/*
		 * Returns the child nodes for each node; also checks if the child is already 
		 * visited and is cost effective (for UCS).
		 */
		public List<Node> getChildnodes()
		{
			Map<String, Integer> childNodes = graph.get(this.getState());
			List<Node> child = new ArrayList<Node>();

			for (Map.Entry<String, Integer> entry : childNodes.entrySet())
			{
				if(!isVisited(entry.getKey()))
				{
					child.add(new Node(entry.getKey(), this.getState(), 
							this.getDepth()+1, this.getCost()+entry.getValue()));
				}
				else if(searchType == UNIFORM_COST_SEARCH)
				{
					updateCostEfficientPath(new Node(entry.getKey(), this.getState(), 
							this.getDepth()+1, this.getCost()+entry.getValue()));
				}
			}

			return child;
		}

		public String toString() 
		{
			return "State: " + this.state + " :: Parent: " + this.parent + " :: Depth: " 
					+ this.depth + " :: Cost: " + this.cost + "\n";
		}
		
		public int compareTo(Node node) 
		{
			if(searchType == BREADTH_FIRST_SEARCH)
			{
				// If the depths are equal, sort alphabetically
				int depth = this.getDepth() - node.getDepth();
				return depth==0 ? this.getState().compareTo(node.getState()) : depth;
			}
			else if(searchType == DEPTH_FIRST_SEARCH)
			{
				// If the depths are equal, sort reverse alphabetically
				int depth = this.getDepth() - node.getDepth();
				return depth==0 ? node.getState().compareTo(this.getState()) : depth;
			}
			else if(searchType == UNIFORM_COST_SEARCH)
			{
				// If the costs are equal, sort alphabetically
				int cost = this.getCost() - node.getCost();
				return cost ==0 ? this.getState().compareTo(node.getState()) : cost;
			}
			return 0;
		}
	}

	public static void main(String[] args) throws Exception 
	{
		agent agent = new agent();
		String output = "";
		readInput("input.txt");

		Node node = null;
		try{
			switch(searchType)
			{
			case BREADTH_FIRST_SEARCH:
				node = agent.queueSearch();
				break;
			case DEPTH_FIRST_SEARCH: 
				node = agent.stackSearch();
				break;
			case UNIFORM_COST_SEARCH: 
				node = agent.queueSearch();
				break;

			default : 
				writeOutput("No Such Algorithm!!");
				break;
			}

			if(node == null)
			{
				output = stackTrace.replaceFirst("-", "") + "\nNoPathAvailable";
			}
			else
			{
				output = stackTrace.replaceFirst("-", "") + "\n" + getPath(node) + "\n" + node.getCost();
			}
			writeOutput(output);

		}catch(Exception e)
		{
			writeOutput("There was an exception \n" + e.getStackTrace());
		}
	}

	//Includes both BFS and UCS
	public Node queueSearch()
	{
		List<Node> childNodes;
		nodeQueue.offer(new Node(source, null, 0, 0));
		Node currNode = null;

		while(!nodeQueue.isEmpty())
		{
			currNode = nodeQueue.poll();
			stackTrace += "-" + currNode.getState();
			visitedNodes.offer(currNode);

			//Goal Test
			if(currNode.getState().equalsIgnoreCase(dest))
			{
				return currNode;
			}

			childNodes = currNode.getChildnodes();
			nodeQueue.addAll(childNodes);

			//Queuing Function:
			//BFS: Sort based on depth and state
			//UCS: Sort based on cost and state
			Collections.sort((List<Node>) nodeQueue);
		}

		return null;
	}

	//DFS
	public Node stackSearch()
	{
		nodeStack.push(new Node(source, null, 0, 0));

		List<Node> childNodes;
		Node currNode = null;

		while(!nodeStack.isEmpty())
		{
			currNode = nodeStack.pop();
			stackTrace += "-" + currNode.getState();
			visitedNodes.offer(currNode);

			//Goal Test
			if(currNode.getState().equalsIgnoreCase(dest))
			{
				return currNode;
			}

			childNodes = currNode.getChildnodes();
			nodeStack.addAll(childNodes);

			//Queuing Function
			//Sort based on depth and state
			Collections.sort((List<Node>) nodeStack);
		}

		return null;
	}

	public static void readInput(String inputFile) throws FileNotFoundException
	{
		Map<String, Integer> paths;
		List<String> nodeNames = new ArrayList<String>();
		Scanner in = new Scanner(new File(inputFile));

		searchType = Integer.parseInt(in.nextLine());
		source = in.nextLine();
		dest = in.nextLine();
		String adjacencyRow = "";

		int nodeCount = Integer.parseInt(in.nextLine());

		for(int i =0; i<nodeCount; i++)
		{
			nodeNames.add(in.nextLine());
		}

		for(int i =0; i<nodeCount; i++)
		{
			paths = new HashMap<String, Integer>();
			adjacencyRow = in.nextLine();

			for(int j =0; j<nodeCount; j++)
			{
				if(Integer.parseInt(adjacencyRow.split("\\s")[j])!=0)
				{
					paths.put(nodeNames.get(j), Integer.parseInt(adjacencyRow.split("\\s")[j]));
				}
			}

			graph.put(nodeNames.get(i), paths);
		}

		in.close();
	}

	public static void writeOutput(String output) throws IOException
	{
		PrintWriter fileWriter = new PrintWriter("output.txt");
		fileWriter.print(output);
		fileWriter.close();
	}

	public static boolean isVisited(String nodeName)
	{
		Queue<Node> queue = new LinkedList<Node>(visitedNodes);
		queue.addAll(nodeQueue);
		queue.addAll(nodeStack);
		
		for(Node node : queue)
		{
			if(node.getState().equalsIgnoreCase(nodeName))
			{
				return true;
			}
		}
		return false;
	}

	public static void updateCostEfficientPath(Node n) 
	{
		for(Node node : nodeQueue)
		{
			if(node.getState().equalsIgnoreCase(n.getState()) && n.getCost()<node.getCost())
			{
				node.setCost(n.getCost());
				node.setParent(n.getParent());
				node.setDepth(n.getDepth());
			}
		}
	}

	public static String getPath(Node n)
	{
		Node currentNode = n;
		String trace = currentNode.getState();

		while(currentNode.getParent()!=null)
		{
			trace = currentNode.getParent() +  "-" + trace;

			for(Node node : visitedNodes)
			{
				if(node.getState().equalsIgnoreCase(currentNode.getParent()))
				{
					currentNode = node;
					break;
				}
			}
		}
		return trace;
	}
}